<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Registration Form</title>
        <style>
            body{
                background: linear-gradient(to bottom right,#FF7738,#FFC06F,#FFEDAF,#FFFFFF);
            }
            h1{
                text-align: center;
            }
            .footer{
                height: 100px;
            }
            @media (max-width: 991px) {
                    .form-group {
                    padding-left:0;
                    padding-right: 0;
                }
            }
            .remove-left{
                padding-left: 0 !important;
                margin-left: 0 !important;
            }
            .remove-right{
                padding-right: 0 !important;
                margin-right: 0 !important;
            }
            .error{
                color: #FF0000;
            }
        </style>

    </head>
    <body>


        <?php
            // define variables and set to empty values
            $fnameErr = $mnameErr = $lnameErr = $emailErr = $phnoErr = $passErr = "";
            $repassErr = $genderErr = $dobErr = "";
            $fname = $mname = $lname = $email = $gender = $phno = $pass = $repass = $dob = "";

            if ($_SERVER["REQUEST_METHOD"] == "POST") 
            {
                //First Name Validation
                if (empty($_POST["fname"])) 
                {
                    $fnameErr = "First Name is required";
                } 
                else 
                {
                    $fname = test_input($_POST["fname"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$fname)) 
                    {
                        $fnameErr = "Only letters and white space allowed"; 
                    }
                }

                //Middle Name Validation
                if (empty($_POST["mname"])) 
                {
                    $mnameErr = "";
                } 
                else 
                {
                    $mname = test_input($_POST["mname"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$mname)) 
                    {
                        $mnameErr = "Only letters and white space allowed"; 
                    }
                }

                //Last Name Validation
                if (empty($_POST["lname"])) 
                {
                    $lnameErr = "First Name is required";
                } 
                else 
                {
                    $lname = test_input($_POST["lname"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$lname)) 
                    {
                        $lnameErr = "Only letters and white space allowed"; 
                    }
                }
                
                //Email Validation
                if (empty($_POST["email"])) 
                {
                    $emailErr = "Email is required";
                } 
                else 
                {
                    $email = test_input($_POST["email"]);
                    // check if e-mail address is well-formed
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                    {
                        $emailErr = "Invalid email format"; 
                    }
                }
                    
                //Phone No Validation
                if (empty($_POST["phno"])) 
                {
                    $phnoErr = "Phone No Should Not Be Empty";
                } 
                else 
                {
                    $phno = test_input($_POST["phno"]);
                    // check if the phone no has exactly 10 digits or not
                    if (!preg_match("/^[0-9]{10}$/",$phno)) 
                    {
                        $phnoErr = "Invalid Phone No. It Should Be 10 Digit No. "; 
                    }
                }

                //Password Validation
                if (empty($_POST["pwd"])) 
                {
                    $passErr = "Password is required";
                } 
                else 
                {
                    $pass = test_input($_POST["pwd"]);
                    // check if password satisfies all conditions
                    if (!preg_match("(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!]).{6,20}",$pass)) 
                    {
                        $passErr = "Invalid Password"; 
                    }
                }

                //Retype Password Validation
                if (empty($_POST["repwd"])) 
                {
                    $repassErr = "Re Type Password is required";
                } 
                else 
                {
                    $repass = test_input($_POST["pwd"]);
                    // chec if retyped password matches the previous password or not
                    if ($repass!=$pass)
                    {
                        $repassErr = "Password Not Matching The Previous Password"; 
                    }
                }

                //Gender Validation
                if (empty($_POST["gender"])) 
                {
                    $genderErr = "Gender is required";
                } 
                else 
                {
                    $gender = test_input($_POST["gender"]);
                }
                
                //Date Of Birth Validation
                if(empty($_POST["dob"]))
                {
                    $dobErr = "Choose A date";
                }
                $dob = $_POST["dob"];
                /*else
                {
                    $dob = $_POST["dob"];
                    $dob_arr  = explode('/', $dob);
                    if (count($dob) == 3) 
                    {
                        if (!checkdate($dob_arr[0], $dob_arr[1], $dob_arr[2])) 
                        {
                            $dobErr = "Invalid Date";
                        }
                    } 
                    else 
                    {
                        $dobErr = "Invalid Date";
                    }
                }*/


                $padhno = $padcit = $padsta = $padcon = $padzip = "";
                $cadhno = $cadcit = $cadsta = $cadcon = $cadzip = "";
                $padhnoErr = $padcitErr = $padstaErr = $padconErr = $padzipErr = "";
                $cadhnoErr = $cadcitErr = $cadstaErr = $cadconErr = $cadzipErr = "";


                // Permanent Address Validation


                // House No

                if (empty($_POST["padd_hno"])) 
                {
                    $padhnoErr = "House No  is required";
                } 
                else 
                {
                    $padhno = test_input($_POST["padd_hno"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[0-9a-zA-Z ]*$/",$padhno)) 
                    {
                        $padhnoErr = "Only letters, numbers and white space allowed"; 
                    }
                }

                // City

                if (empty($_POST["padd_city"])) 
                {
                    $padcitErr = "City is required";
                } 
                else 
                {
                    $padcit = test_input($_POST["padd_city"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$padcit)) 
                    {
                        $padcitErr = "Only letters and white space allowed"; 
                    }
                }

                // State

                if (empty($_POST["padd_state"])) 
                {
                    $padstaErr = "State is required";
                } 
                else 
                {
                    $padsta = test_input($_POST["padd_state"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$padsta)) 
                    {
                        $padstaErr = "Only letters and white space allowed"; 
                    }
                }

                // Country

                if (empty($_POST["padd_con"])) 
                {
                    $padconErr = "Country is required";
                } 
                else 
                {
                    $padcon = test_input($_POST["padd_con"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$padcon)) 
                    {
                        $padconErr = "Only letters and white space allowed"; 
                    }
                }

                // Zip Code
                if (empty($_POST["padd_zip"])) 
                {
                    $padzipErr = "Zip Code is required";
                } 
                else 
                {
                    $padzip = test_input($_POST["padd_zip"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("^[0-9]{6}$",$padzip)) 
                    {
                        $padzipErr = "Only Numbers allowed"; 
                    }
                }



                // Current Address Validation


                // House No

                if (empty($_POST["cadd_hno"])) 
                {
                    $cadhnoErr = "House No  is required";
                } 
                else 
                {
                    $cadhno = test_input($_POST["cadd_hno"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[0-9a-zA-Z ]*$/",$cadhno)) 
                    {
                        $cadhnoErr = "Only letters, numbers and white space allowed"; 
                    }
                }

                // City

                if (empty($_POST["cadd_city"])) 
                {
                    $cadcitErr = "City is required";
                } 
                else 
                {
                    $cadcit = test_input($_POST["cadd_city"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$cadcit)) 
                    {
                        $cadcitErr = "Only letters and white space allowed"; 
                    }
                }

                // State

                if (empty($_POST["cadd_state"])) 
                {
                    $cadstaErr = "State is required";
                } 
                else 
                {
                    $cadsta = test_input($_POST["cadd_state"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$cadsta)) 
                    {
                        $cadstaErr = "Only letters and white space allowed"; 
                    }
                }

                // Country

                if (empty($_POST["cadd_con"])) 
                {
                    $cadconErr = "Country is required";
                } 
                else 
                {
                    $cadcon = test_input($_POST["cadd_con"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$cadcon)) 
                    {
                        $cadconErr = "Only letters and white space allowed"; 
                    }
                }

                // Zip Code
                if (empty($_POST["cadd_zip"])) 
                {
                    $cadzipErr = "Zip Code is required";
                } 
                else 
                {
                    $cadzip = test_input($_POST["cadd_zip"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("^[0-9]{6}$",$cadzip)) 
                    {
                        $cadzipErr = "Only Numbers allowed"; 
                    }
                }


                // Personal Interest Validation
                
                $hobErr = "";
                if(!isset($_POST['hobbies']))
                {
                    $hobErr = "Please Choose One Hobby"; 
                }



            }
                

            function test_input($data) 
            {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>



        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h1>Registration Form</h1><br>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="form-group">
                        <label for="email"><span class="error">* required fields</span></label>
                    </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12 remove-left">
                            <label for="email">First Name<span class="error">*</span></label>
                            <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $fname ?>">
                            <label for="email" class="error"><?php echo $fnameErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 ">
                            <label for="email">Middle Name</label>
                            <input type="text" class="form-control" id="mname" name="mname" value="<?php echo $mname ?>">
                            <label for="email" class="error"><?php echo $mnameErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 remove-right">
                            <label for="email">Last Name<span class="error">*</span></label>
                            <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $lname ?>">
                            <label for="email" class="error"><?php echo $lnameErr; ?></label>
                        </div>
                        <br>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="email">Email address<span class="error">*</span></label>
                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $email ?>">
                            <label for="email" class="error"><?php echo $emailErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="email">Phone No<span class="error">*</span></label>
                            <input type="number" class="form-control" id="phno" name="phno" value="<?php echo $phno ?>">
                            <label for="email" class="error"><?php echo $phnoErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password<span class="error">*</span></label>
                            <input type="password" class="form-control" id="pwd" name="pwd" data-toggle="tooltip" title="Password Should Contain Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character">
                            <label for="email" class="error"><?php echo $passErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Retype Password<span class="error">*</span></label>
                            <input type="password" class="form-control" id="repwd" name="repwd">
                            <label for="email" class="error"><?php echo $repassErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 remove-left">
                            <label for="email">Gender<span class="error">*</span></label><br>
                            <label class="radio-inline"><input type="radio" name="gender" value="male" <?php if (isset($gender) && $gender=="male") echo "checked";?>>Male</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="female" <?php if (isset($gender) && $gender=="female") echo "checked";?>>Female</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="others" <?php if (isset($gender) && $gender=="others") echo "checked";?>>Others</label><br>
                            <label for="email" class="error"><?php echo $genderErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 remove-right">
                            <label for="email">DOB<span class="error">*</span></label><br>
                            <input type="date" class="form-control" id="dob" name="dob" value="<?php echo $dob ?>">
                            <label for="email" class="error"><?php echo $dobErr; ?></label>
                        </div>

                        <div class="form-group">
                            <label></label>
                        </div>

                        <div class="form-group">
                            <label for="email">Permanent Address<span class="error">*</span></label><hr>
                        </div>   
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">House No(Separate with '-')</label>
                            <input type="text" class="form-control" id="padd_hno" name="padd_hno" value="<?php echo $padhno; ?>">
                            <label for="email" class="error"><?php echo $padhnoErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">City</label>
                            <input type="text" class="form-control" id="padd_city" name="padd_city" value="<?php echo $padcit; ?>">
                            <label for="email" class="error"><?php echo $padcitErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">State</label>
                            <input type="text" class="form-control" id="padd_state" name="padd_state" value="<?php echo $padsta; ?>">
                            <label for="email" class="error"><?php echo $padstaErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Country</label>
                            <input type="text" class="form-control" id="padd_con" name="padd_con" value="<?php echo $padcon; ?>">
                            <label for="email" class="error"><?php echo $padconErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Zip Code</label>
                            <input type="text" class="form-control" id="padd_zip" name="padd_zip" value="<?php echo $padzip; ?>">
                            <label for="email" class="error"><?php echo $padzipErr; ?></label>
                        </div>

                        <div class="form-group">
                            <label></label>
                        </div>
                        <div class="form-group">
                            <label></label>
                        </div>
                        
                        <div class="form-group">
                            <label for="email">Current Address<span class="error">*</span></label><hr>
                        </div>   
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">House No(Separate with '-')</label>
                            <input type="text" class="form-control" id="cadd_hno" name="cadd_hno" value="<?php echo $cadhno; ?>">
                            <label for="email" class="error"><?php echo $cadhnoErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">City</label>
                            <input type="text" class="form-control" id="cadd_city" name="cadd_city" value="<?php echo $cadcit; ?>">
                            <label for="email" class="error"><?php echo $cadcitErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">State</label>
                            <input type="text" class="form-control" id="cadd_state" name="cadd_state" value="<?php echo $cadsta; ?>">
                            <label for="email" class="error"><?php echo $cadstaErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Country</label>
                            <input type="text" class="form-control" id="cadd_con" name="cadd_con" value="<?php echo $cadcon; ?>">
                            <label for="email" class="error"><?php echo $cadconErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Zip Code</label>
                            <input type="text" class="form-control" id="cadd_zip" name="cadd_zip" value="<?php echo $cadzip; ?>">
                            <label for="email" class="error"><?php echo $cadzipErr; ?></label>
                        </div>
                    
                        <div class="form-group">
                            <label>Personal Interests<span class="error">*</span></label><br>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies" value="sports" id="hobbies"><span class="pins"> : Sports</label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies" value="books" id="hobbies"><span class="pins"> : Books</label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies" value="comp_soft" id="hobbies"><span class="pins"> : Computer And Software</label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies" value="fashion" id="hobbies"><span class="pins"> : Fashion</label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies" value="photography" id="hobbies"><span class="pins"> : Photograhy</label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies" value="cooking" id="hobbies"><span class="pins"> : Cooking</label>
                            </div>
                            <div class="checkbox">
                                <label for="email" class="error" style="padding-left:0;"><?php echo $hobErr; ?></label>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                            <button type="submit" class="btn btn-primary" name="submit">Register</button>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                            <button type="reset" class="btn">Reset</button>
                        </div>
                    </form>
                
                </div>
            </div>
        </div>
        <div class="footer">
        </div>   
    </body>
</html>
